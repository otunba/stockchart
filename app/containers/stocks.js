import React,{ Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import StockList from '../components/stockLists';
import { fetchStock, getUserValue, deleteStock, fetchStockChart, selectStock, watchStock} from '../actions/stockActions';
import Header from '../components/header';
import styles from '../components/Home.css';
import StockItem from '../components/stockItem';
import Chart from '../components/chart';
import AdUnit from '../components/dfp';
import Footer from '../components/footer';


class Stocks extends Component{
     constructor(props){
       super(props);
       this.getStocks = this.getStocks.bind(this);
       this.setSymbol = this.setSymbol.bind(this);
       this.getSelectedStock = this.getSelectedStock.bind(this);
       this.getStockChart = this.getStockChart.bind(this);
       this.toggleWatch = this.toggleWatch.bind(this);
       this.poll = 0;
     }

    getStocks(e){
      e.preventDefault();
      this.props.fetchStock(this.props.userInput);
      this.props.getUserValue('');
    }


  setSymbol(e){
     this.props.getUserValue(e.target.value);
  }
// for potential ads <AdUnit ddbId="adslot4" ddbUnit='4756/komo/Web/news/8' ddbSize={[300, 250]}/>
  toggleWatch(stock){
    var self = this;
    this.props.watchStock(stock);
    if(this.props.stockBool){
      (function(){  self.poll = setInterval(()=> { self.props.fetchStock(stock.Symbol); self.props.fetchStockChart(stock)}, 100000)})();
    }
   else{
      clearInterval(this.poll)
    }
  }

  getSelectedStock(item){
    this.props.deleteStock(item);
  }

getStockChart(stockSymbol){
  this.props.fetchStockChart(stockSymbol);
  this.props.selectStock(stockSymbol);
}



  render(){
       return(
         <div>
           <div className = { styles.container }>
             { this.props.isFormEnabled ?
               <Header
                   userValFnc = { this.setSymbol }
                   symbolFnc = { this.getStocks }
                   userVal = { this.props.userInput } />
                 : <div className={styles.warning}> You have reached your max limit, please upgrade! </div> }
                 <section>
                  { this.props.stockChart !== null  && this.props.selectedStock !== null ?  <Chart chart={ this.props.stockChart } selectedStock={ this.props.selectedStock }/> : '' }
                 </section>
             <div>
               <StockItem deleteStock = { this.getSelectedStock }
                          watchStock = { this.toggleWatch }
                          stockObj = { this.props.stockData }
                          stockBool = { this.props.stockBool}
                          showChart = { this.getStockChart }
                          stock={this.selectStock} />
             </div>
           </div>
         </div>
       )
     }

}


function mapStateToProps(state) {
  return {
    userInput: state.userStockReducer.inputFromUser,
    stockData: state.stockReducer,
    isFormEnabled: state.stockReducer.isFormEnabled,
    stockChart: state.stockReducer.chart,
    stockBool: state.stockReducer.isWatch,
    selectedStock: state.stockReducer.selectedStock
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({fetchStock:fetchStock, getUserValue:getUserValue, deleteStock:deleteStock, fetchStockChart:fetchStockChart, selectStock:selectStock , watchStock:watchStock}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Stocks);
