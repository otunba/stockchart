import axios from 'axios';
import * as types from './constants';

const API_URL = 'http://dev.markitondemand.com/MODApis/Api/v2/Quote/json?';
const CHART_API_URL = 'http://dev.markitondemand.com/MODApis/Api/v2/InteractiveChart/json?';



export function fetchStock(arg){
     const param = `symbol=${arg}`;
     return {
        type: types.GET_STOCK,
        payload: axios.get(`${API_URL}${param}`).then((response) =>{ return response })
     }
}


export function watchStock(stock){
   return{
        type: types.WATCH_STOCK,
        payload: stock
       }

}




export function deleteStock(stockId) {
  return {
    type: types.DELETE_STOCK,
    payload: stockId
  }
}

export function getUserValue(symbol) {
  return {
    type: types.USER_STOCK,
    payload: symbol
  }
}

export function fetchStockChart(stock){
       const symbol = stock.Symbol;
       var objString = JSON.stringify({"Normalized":false,"NumberOfDays":365,"DataPeriod":"Day",
                     "Elements":[{"Symbol":`${symbol}`,"Type":"price","Params":["ohlc"]},{"Symbol":`${symbol}`,"Type":"volume"}]});
       var paramObj =`parameters=${objString}`;
              return{
                type: types.SHOW_STOCK_CHART,
                payload: axios.get(`${CHART_API_URL}${paramObj}`).then((response)=>{ return response})
              }
}

export function selectStock(data) {
      return{
        type: types.SELECT_STOCK,
        payload: data
      }
}
