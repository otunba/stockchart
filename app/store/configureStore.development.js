import { createStore, applyMiddleware, compose } from 'redux';
//import thunkMiddleware from 'redux-thunk';
import promise from 'redux-promise';
import { hashHistory } from 'react-router';
import { routerMiddleware, push } from 'react-router-redux';
import createLogger from 'redux-logger';
import rootReducer from '../reducers';

import * as stocksActions from '../actions/stockActions';


const actionCreators = {
  ...stocksActions,
  push
};

const logger = createLogger({
  level: 'info',
  collapsed: true
});

const router = routerMiddleware(hashHistory);

const enhancer = compose(
  applyMiddleware(promise, router, logger),
  window.devToolsExtension ?
    window.devToolsExtension({actionCreators}) :
    noop => noop
);

export default function configureStore(initialState) {
  const store = createStore(rootReducer, initialState, enhancer);

  //console.log('store', store);

  if (window.devToolsExtension) {
    window.devToolsExtension.updateStore(store);
  }

  if (module.hot) {
    module.hot.accept('../reducers', () =>
      store.replaceReducer(require('../reducers')) // eslint-disable-line global-require
    );
  }

  return store;
}
