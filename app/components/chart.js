import React,{ Component } from 'react';
import HighStock from 'react-highcharts/ReactHighStock';
import { getConfig, colorSwitcher } from  '../utils/chartUtil';
import { numberSwitch } from '../utils/numerals';
import numeral from 'numeral';
import styles from './stocks.css';
import NumberFormat from 'react-number-format';


const StockChart = (props) => {

    const stockChart =  getConfig(props.chart, props.selectedStock);
    return (<div>
             <div className={styles.chartContainer}>
               <HighStock config={stockChart}></HighStock>
             </div>
             <div className={styles.verboseStock}>
               <div className={styles.verboseBox}>
                 <small className={styles.stockValue}>
                   {numberSwitch(props.selectedStock.Volume).toUpperCase()}
                 </small>
                <small className={styles.small}>Volume </small>
               </div>
               <div className={styles.verboseBox}>
                  <small className={styles.stockValue}>
                    $ {numberSwitch(props.selectedStock.MarketCap).toUpperCase()}
                  </small>
                  <small className={styles.small}>Market Cap</small>
               </div>
               <div className={styles.verboseBox}>
                 <small className={styles.stockValue}>
                    <li className={colorSwitcher(props.selectedStock.ChangePercentYTD).direction}> </li>
                     {props.selectedStock.ChangePercentYTD.toFixed(1)} %
                 </small>
                 <small className={styles.small}> YTD </small>
               </div>
               <div className={styles.bar}></div>
               </div>
            </div>
          )

}


export default StockChart;
