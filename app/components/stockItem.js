import React from 'react';
import StockView from './stockLists';



const StockItem = (props) =>{
  const showStocks = props.stockObj.stocks.map((stock,id) =>{
    return <StockView deleteStock={props.deleteStock}
                      key={id}
                      stockObj={stock}
                      watchStock={props.watchStock}
                      showChart={props.showChart}
                      stockBool={props.stockBool} />
  });

  return(
    <div>
      { props.stockObj.stocks.length == 0 ? '' :  showStocks }
      </div>
    )

  }


export default StockItem;
