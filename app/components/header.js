import React from 'react';
import styles from './Home.css';

const Header = (props) =>{
  const { userValFnc, symbolFnc, userVal } = props
  return(
    <div>
     <header className={styles.header}>
       <div className={styles.formWrapper}>
         <div className={styles.containers}>
           <form onSubmit={symbolFnc}>
             <input type="search"
                     className={styles.search}
                     placeholder="Enter Stock Symbol"
                     value={userVal}
                     onChange={userValFnc} />
               <button type="sumbit" className={styles.icon}>
                 <i className="fa fa-search"></i>
               </button>
             </form>
          </div>
       </div>
      </header>
    </div>
  )

}

export default Header;
