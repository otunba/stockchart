import React from  'react';
import styles from './stocks.css';
import Trend from './trendsView';
import { toggleStockColor,trendDataProvider } from '../utils/logicHelper';




const StockView = (props) =>{

    const { stockObj, deleteStock, showChart, watchStock, stockBool } = props;
    return(
      <div>
        <li className={styles.listcontainer}>
          { stockObj.isActive ?
             <i className={`${"fa  fa-bolt"}${ stockBool ? "" : " blink "}`} onClick={()=> { return  watchStock(stockObj) } }></i>
           : ''
          }
          <div className={[`${styles.listwrapper} ${stockObj.isActive ? styles.active : '' }`]} onClick={() =>{  return stockBool ? (showChart(stockObj)) : null  } }>
          <div className={styles.stockWrap}>
             <span className={styles.symbol}> {stockObj.Symbol}  </span>
             <small className={styles.stockName}> {stockObj.Name} </small>
          </div>
          <div className={styles.trendWrap}>
            <Trend stockData={ trendDataProvider(stockObj) } color={ toggleStockColor(stockObj.Change, null) } />
            <small className={styles.stockName}> Current Trend </small>
          </div>
          <div className={styles.priceWrap}>
           <span className={styles.lastPrice, toggleStockColor(stockObj.Change,styles)}> ${stockObj.LastPrice}</span>
           <small className={styles.stockName}> Current Price </small>
         </div>
         </div>
         {!stockObj.isActive ?
                <button onClick={()=> { return deleteStock(stockObj) } } className={styles.icon}>
                    <i className="fa  fa-trash"></i>
                  </button> : ''}

      </li>
      </div>
    )
}


export default StockView;
