import React from 'react';
import { Sparklines, SparklinesLine,SparklinesCurve,SparklinesSpots, SparklinesReferenceLine } from 'react-sparklines';


const Trend = (props) =>{
   const { color, stockData } = props;

   return(
      <Sparklines data={stockData} limit={9} width={80} height={20} margin={2}>
        <SparklinesCurve color={color} style={{ fill: "none" }}   />
        <SparklinesReferenceLine type="mean" />
        <SparklinesSpots />
      </Sparklines>
   )
};

export default Trend;
