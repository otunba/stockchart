import React from 'react';
import style from './Home.css';




const Footer = (props) =>{
  return (
    <div>
      <footer>
        <div className={style.footertext}>
        Handcrafted By CodeJunkie.us &copy; 2017
        </div>
      </footer>
    </div>
  )

}


export default Footer;
