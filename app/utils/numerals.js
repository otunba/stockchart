
export function numberSwitch(value){

  if (value <= 1000) {
       return value.toString();
   }

   const numDigits = (""+value).length;
   const suffixIndex = Math.floor(numDigits / 3);

   const normalisedValue = value / Math.pow(1000, suffixIndex);

   let precision = 2;
   if (normalisedValue < 1) {
       precision = 1;
   }

   const suffixes = ["", "k", "m", "b","t"];
   return normalisedValue.toPrecision(precision) + suffixes[suffixIndex];
}
