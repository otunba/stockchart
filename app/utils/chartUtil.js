
function fixDate(dateIn){

  var dat = new Date(dateIn);

  return Date.UTC(dat.getFullYear(),dat.getMonth(), dat.getDate());

}

export function colorSwitcher(stockChange){
   var negative = {
        top: 'rgba(214, 71, 89,0.5)',
        bottom: 'rgba(210, 118, 80,0.1)',
        color: '#da7352',
        direction:'fa fa-caret-down'
   }

   var positive = {
       top: 'rgba(0, 83, 126, 0.5)',
       bottom: 'rgba(43, 49, 62, 0.1)',
       color: '#3AA17E',
       direction: 'fa fa-caret-up'

   }

   if(stockChange.toFixed(2) < 0 ){
     return negative;
   }
   else{
     return positive;
   }
}

 function getOHLC(data){
  var dates = data.Dates || [];
  var elements = data.Elements || [];
  var chartSeries = [];

  if (elements[0]){

        for (var i = 0, datLen = dates.length; i < datLen; i++) {
            var dat = fixDate( dates[i] );
            var pointData = [
                dat,
                elements[0].DataSeries['open'].values[i],
                elements[0].DataSeries['high'].values[i],
                elements[0].DataSeries['low'].values[i],
                elements[0].DataSeries['close'].values[i]
            ];
            chartSeries.push( pointData );
        };
    }
    return chartSeries;


}

function getVolume(data){
    var dates = data.Dates || [];
    var elements = data.Elements || [];
    var chartSeries = [];

    if (elements[1]){
        for (var i = 0, datLen = dates.length; i < datLen; i++) {
             var dat = fixDate( dates[i] );
             var pointData = [dat, elements[1].DataSeries['volume'].values[i] ];
             chartSeries.push( pointData );
        };
    }
    return chartSeries;
}

export function getConfig(data, stock){
  var ohlc = getOHLC(data);
  var volume = getVolume(data);

  return {
    rangeSelector:{
      selected: 4,
            inputEnabled: false,
            buttonTheme: {
                visibility: 'hidden'
            },
            labelStyle: {
                visibility: 'hidden'
            }
},
    chart: {
      backgroundColor:null,
      marginright: -55,
      width: 416,
      animation: false
   },
   navigator: {
            enabled: false,
            height: 0
  },
   plotOptions:{
          area:{
            lineWidth: 1,
            marker: {
              enabled: false,
              states:{
                hover:{
                  enabled: true,
                  radius: 5
                }
              }
            },
            shadow: false,
            states:{
              hover:{
                lineWidth: 2
              }
            }
          }
    },
    title:{
      text:  `<i class=" ${colorSwitcher(stock.Change).direction}"> </i> ${stock.Name}`,
      useHTML: true,
      align:'center',
      style: {
            color: 'rgba(167, 149, 142, 0.7)',
            fontWeight: 'lighter',
            fontFamily:'geometria_lightlight',
            fontSize: '20px'
         }
    },
    yAxis: {
      title: {
          text: 'Price',
          style:{
             color: colorSwitcher(stock.Change).color
          }
      },
       min: 0,
       gridLineWidth: 0,
       gridLineDashStyle: 'longdash',
       tickColor: colorSwitcher(stock.Change).color,
       tickLength: 5,
       tickWidth: 0,
       gridLineWidth: 0,
       tickPosition: 'outside',
       labels: {
           align: 'left',
           x:-10,
           y: 5,
           style: {
                 color: colorSwitcher(stock.Change).color,
                 fontWeight: 'normal',
                 fontSize: '10px'
              }
       },
       lineWidth:0.3,
       lineColor: colorSwitcher(stock.Change).color
   },
    xAxis: [{
            tickColor: colorSwitcher(stock.Change).color,
            gridLineWidth: 0,
             lineColor: colorSwitcher(stock.Change).color,
             labels:{
               style: {
                     color: colorSwitcher(stock.Change).color,
                     fontWeight: 'normal',
                     fontSize: '10px'
                 }
            },
            height: 178,
            lineWidth: 0.5
        }, {
            title: {
                text: 'Volume'
            },
            top: 400,
            height: 100,
            offset: 0,
            lineWidth: 2
        }],

        series: [{
            animation: false,
            name: data.Elements[0].Symbol,
            data: ohlc,
            color: colorSwitcher(stock.Change).color,
            type: "area",
            fillColor : {
             linearGradient : [10, 50, 0, 300],
             stops : [
               [0.1, colorSwitcher(stock.Change).top],
               [0.9, colorSwitcher(stock.Change).bottom],
             ]
           },
            tooltip: {
              valueDecimals: 2
            }
          }],
        credits: {
            enabled:false
        }
  }

}
