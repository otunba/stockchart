
export function toggleStockColor(change,styles){
      if(styles == null){
        return  change.toFixed(2) < 0 ? '#da7352' : '#3AA17E';
      }
      else{
          return change.toFixed(2) < 0 ? styles.negative : styles.positive;
      }

}

//  stockData = [open,High,low,lastPrice]
export function trendDataProvider (stockData){
         let trend = [];

         for(var k in stockData){
            if(k == 'Open' || k =='High' || k == 'Low' || k == 'LastPrice'){
              trend.push(stockData[k]);
            }

         }
  return trend;

}


export function filterArr(arr){
return ( arr.filter( (el) =>{
         //console.log(el)
       return (el !== undefined);
     })
)
}


function filterUndefined(arr){
  arr.filter( (element) =>{
    return element !== undefined;
  });
  return arr;
}

export function removeDuplicates(arr, prop) {
     var new_arr = [];
     var lookup  = {};

     for (var i in arr) {
         lookup[arr[i][prop]] = arr[i];
     }

     for (i in lookup) {
         new_arr.push(lookup[i]);
     }

     return new_arr;
 }
