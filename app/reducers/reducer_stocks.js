import { GET_STOCK, SELECT_STOCK, DELETE_STOCK, ENABLE_DISABLE_STOCK_FORM, WATCH_STOCK, MAX_STOCKS_ALLOWED, TOGGLE_WATCH_STOCK, SHOW_STOCK_CHART } from '../actions/constants';
import { removeDuplicates } from '../utils/logicHelper';

export default function (state = { stocks: [], selectedStock: null, isFormEnabled: true, chart: null, isWatch: true, prevState: null}, action) {
  switch (action.type) {
    case GET_STOCK:
      if(action.payload.data.Message == undefined && action.payload.data.Status == 'SUCCESS'){
         action.payload.data.autowatch = true;
          var newState,
          newArr = [];
         if( state.stocks.length !== 0 ){
          if(state.prevState !== null){
             if(state.prevState.Symbol == action.payload.data.Symbol){
                // the stock is active,
                 action.payload.data.isActive = true;
                var data = Object.assign({}, state, { stocks : state.stocks.concat([action.payload.data]),
                       selectedStock:action.payload.data,
                       isFormEnabled: state.stocks.length === MAX_STOCKS_ALLOWED ? !state.isFormEnabled : state.isFormEnabled
               });

               var user = removeDuplicates(data.stocks,'Symbol');
               data.stocks = user
               newState = data;
          }
          else{
            var data = Object.assign({}, state, { stocks : state.stocks.concat([action.payload.data]),
                   //selectedStock:action.payload.data,
                   isFormEnabled: state.stocks.length === MAX_STOCKS_ALLOWED ? !state.isFormEnabled : state.isFormEnabled
           });

           var user = removeDuplicates(data.stocks,'Symbol');

           data.stocks = user
           newState = data;
          }
     }
     else {
       if(state.selectedStock != null && state.selectedStock.isActive == true && ( state.selectedStock.Symbol == action.payload.data.Symbol)){
         action.payload.data.isActive = true;
       }
       var data = Object.assign({}, state, { stocks : state.stocks.concat([action.payload.data]),
              //selectedStock:action.payload.data,
              isFormEnabled: state.stocks.length === MAX_STOCKS_ALLOWED ? !state.isFormEnabled : state.isFormEnabled
      });

      var user = removeDuplicates(data.stocks,'Symbol');

      data.stocks = user
      newState = data;
     }
          return newState;
         }
         else if(state.stocks.length == 0){
             var data = Object.assign({}, state, { stocks : state.stocks.concat([action.payload.data]),
               isFormEnabled: state.stocks.length === MAX_STOCKS_ALLOWED ? !state.isFormEnabled : state.isFormEnabled
             });
            newState  =  data;
            return newState;
           }


      }
      else {
        return state;
      }


    case SHOW_STOCK_CHART:
      return Object.assign({}, state, { chart: action.payload.data });

    case WATCH_STOCK:
      return Object.assign({}, state, { isWatch: !state.isWatch, prevState: action.payload });


    case SELECT_STOCK:
    for(let i = 0; i < state.stocks.length; i++){
      if(action.payload.Symbol == state.stocks[i].Symbol){
        state.stocks[i].isActive = true;
      }
      else{
        state.stocks[i].isActive = false;
      }

    }

      return { ...state, selectedStock: action.payload };

    case DELETE_STOCK:
      for (let i = 0; i < state.stocks.length; i++) {
        if (state.stocks[i].Symbol === action.payload.Symbol && (state.stocks[i] !== undefined)) {
          state.stocks.splice(i, 1);

        if (state.chart.Elements[0].Symbol === action.payload.Symbol && (state.chart !== null)) {
          delete state.chart;
           state.chart = null;
         }
        }
      }

      return  Object.assign({}, state, { stocks : state.stocks, chart: state.chart, isFormEnabled: state.stocks.length === MAX_STOCKS_ALLOWED ? !state.isFormEnabled : state.isFormEnabled });

    default:
      return state;
  }
}
