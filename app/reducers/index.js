import { combineReducers } from 'redux';
import { routerReducer as routing } from 'react-router-redux';
import stockReducer from './reducer_stocks';
import userStockReducer from './reducer_userStock';

const rootReducer = combineReducers({
  stockReducer,
  userStockReducer,
  routing
});


export default rootReducer;
