import { USER_STOCK } from '../actions/constants';

export default function (state ={ inputFromUser:'' }, action) {
  switch (action.type) {
    case USER_STOCK :
      return { ...state, inputFromUser: action.payload }

    default:
      return state;
  }
}
